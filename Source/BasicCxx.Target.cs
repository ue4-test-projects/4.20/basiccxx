using UnrealBuildTool;
using System.Collections.Generic;

public class BasicCxxTarget : TargetRules
{
	public BasicCxxTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		
		ExtraModuleNames.AddRange( new string[] { "BasicCxx" } );
	}
}
